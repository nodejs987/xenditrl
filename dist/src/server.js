"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("source-map-support/register");
require("./module-alias");
const bluebird_1 = __importDefault(require("bluebird"));
const logger_1 = require("src/util/logger");
const app_1 = require("src/app");
const SERVER_SHUTDOWN_TIMEOUT_MS = 10000;
/**
 * Helper function to log an exit code before exiting the process.
 */
const logAndExitProcess = (exitCode) => {
    logger_1.logger.info({
        exit_code_number: exitCode
    }, 'Exiting process');
    process.exit(exitCode);
};
/**
 * Returns a promise that attempts to shut an http server down,
 * resolving if it succeeded and rejecting if it failed or timed out.
 */
const shutdownServerWithTimeout = (server) => __awaiter(void 0, void 0, void 0, function* () {
    return Promise.race([
        bluebird_1.default.fromCallback((cb) => server.close(cb)),
        new Promise((resolve, reject) => setTimeout(() => reject(Error('Timeout shutting server')), SERVER_SHUTDOWN_TIMEOUT_MS))
    ]);
});
/**
 * Helper function to setup signal handlers on the process to gracefully
 * shutdown the server.
 */
const setupSignalHandlers = (server) => {
    process.on('SIGTERM', () => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.logger.info('Received signal: SIGTERM');
        try {
            yield shutdownServerWithTimeout(server);
            logAndExitProcess(0);
        }
        catch (err) {
            logger_1.logger.error(err, 'Failed to shutdown server');
            logAndExitProcess(1);
        }
    }));
    process.on('SIGINT', () => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.logger.info('Received signal: SIGINT');
        try {
            yield shutdownServerWithTimeout(server);
            logAndExitProcess(0);
        }
        catch (err) {
            logger_1.logger.error(err, 'Failed to shutdown server');
            logAndExitProcess(1);
        }
    }));
};
/**
 * Sets up event listeners on unexpected errors and warnings. These should theoretically
 * never happen. If they do, we assume that the app is in a bad state. For errors, we
 * exit the process with code 1.
 */
const setupProcessEventListeners = () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    process.on('unhandledRejection', (reason) => {
        logger_1.logger.warn({ reason_object: reason }, 'encountered unhandled rejection');
        logAndExitProcess(1);
    });
    process.on('uncaughtException', (err) => {
        logger_1.logger.error(err, 'encountered uncaught exception');
        logAndExitProcess(1);
    });
    process.on('warning', (warning) => {
        logger_1.logger.warn({
            warning_object: warning
        }, 'encountered warning');
    });
};
/**
 * Start an Express server and installs signal handlers on the
 * process for graceful shutdown.
 */
(() => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const app = yield app_1.createApp();
        const server = app.listen(app.get('port'), () => {
            logger_1.logger.info({
                port_number: app.get('port'),
                env_string: app.get('env')
            }, 'Started express server');
        });
        setupSignalHandlers(server);
        setupProcessEventListeners();
    }
    catch (err) {
        logger_1.logger.error(err, 'error caught in server.ts');
    }
}))();
//# sourceMappingURL=server.js.map