"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createApp = void 0;
const express_1 = __importDefault(require("express"));
const express_http_context_1 = __importDefault(require("express-http-context"));
const compression_1 = __importDefault(require("compression"));
const init_1 = require("src/init");
const openapi_validator_1 = require("src/util/openapi-validator");
const handle_error_code_1 = require("src/middlewares/handle-error-code");
const rateLimiter_1 = require("src/middlewares/rateLimiter");
function setRoutes(app) {
    return __awaiter(this, void 0, void 0, function* () {
        const { disbursementController, invoiceController, fixedVAController } = yield init_1.init();
        app.use('/api/disbursement', disbursementController.getRouter());
        app.use('/api/invoice', invoiceController.getRouter());
        app.use('/api/fixedVA', fixedVAController.getRouter());
    });
}
function createApp() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = express_1.default();
        app.set('port', process.env.port || 3000);
        app.use(compression_1.default());
        app.use(express_1.default.json({ limit: '5mb', type: 'application/json' }));
        app.use(express_1.default.urlencoded({ extended: true }));
        const openApiValidator = new openapi_validator_1.OpenApiValidator();
        yield openApiValidator.install(app);
        app.use(express_http_context_1.default.middleware);
        app.use(handle_error_code_1.errorHandler());
        app.use(rateLimiter_1.rateLimiter);
        yield setRoutes(app);
        return app;
    });
}
exports.createApp = createApp;
//# sourceMappingURL=app.js.map