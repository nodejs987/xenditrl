"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadConfig = void 0;
const platformRateLimiterConfig = {
    "Disbursements": {
        "license1": { "GET": { configInterval: { points: 2, duration: 10 } },
            "POST": { configInterval: { points: 3, duration: 10 } } },
        "license2": { "GET": { configInterval: { points: 1, duration: 10 } },
            "POST": { configInterval: { points: 2, duration: 10 } } }
    },
    "FixedVA": {
        "license1": { "GET": { configInterval: { points: 2, duration: 10 } },
            "POST": { configInterval: { points: 3, duration: 10 } } },
        "license2": { "GET": { configInterval: { points: 1, duration: 10 } },
            "POST": { configInterval: { points: 2, duration: 10 } } }
    },
    "Invoice": {
        "license1": { "GET": { configInterval: { points: 2, duration: 10 } },
            "POST": { configInterval: { points: 3, duration: 10 } } },
        "license2": { "GET": { configInterval: { points: 1, duration: 10 } },
            "POST": { configInterval: { points: 2, duration: 10 } } }
    }
};
function loadConfig(platform, user, method) {
    let rateLimiterName = platformRateLimiterConfig.hasOwnProperty(platform) && platformRateLimiterConfig[platform].hasOwnProperty(user)
        ? platform + '.' + user + '.' + method : undefined;
    let rateLimiterconfig = platformRateLimiterConfig.hasOwnProperty(platform) && platformRateLimiterConfig[platform].hasOwnProperty(user)
        ? platformRateLimiterConfig[platform][user][method] : undefined;
    return {
        rateLimiterName,
        rateLimiterconfig
    };
}
exports.loadConfig = loadConfig;
//# sourceMappingURL=rateLimiterConfig.js.map