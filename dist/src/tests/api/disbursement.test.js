"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const app_1 = require("src/app");
describe('Disbursement Integration tests', () => {
    let server;
    beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
        server = yield app_1.createApp();
    }));
    describe('/api/disbursement', () => {
        describe('For License 1 it should server more request as compared to license 1', () => {
            it('should return status 200 if we send equal or less number of request specifed for get method for license 1', () => __awaiter(void 0, void 0, void 0, function* () {
                const response = yield supertest_1.default(server).get('/api/disbursement').set({
                    platform: "Disbursements",
                    license: "license1",
                    userId: 1
                });
                expect(response.status).toBe(200);
            }));
            it('should return status 492 if we send more number of request specifed for get method for license 1', () => __awaiter(void 0, void 0, void 0, function* () {
                let response;
                for (let i = 0; i < 7; ++i) {
                    response = yield supertest_1.default(server).get('/api/disbursement').set({
                        platform: "Disbursements",
                        license: "license1",
                        userId: 2
                    });
                }
                expect(response.status).toBe(429);
            }));
            it('should return status 201 if we send equal or less number of request specifed for post method for license 1', () => __awaiter(void 0, void 0, void 0, function* () {
                const response = yield supertest_1.default(server).post('/api/disbursement').set({
                    platform: "Disbursements",
                    license: "license1",
                    userId: 3
                });
                expect(response.status).toBe(201);
            }));
            it('should return status 492 if we send more number of request specifed for post method for license 1', () => __awaiter(void 0, void 0, void 0, function* () {
                let response;
                for (let i = 0; i < 11; ++i) {
                    response = yield supertest_1.default(server).post('/api/disbursement').set({
                        platform: "Disbursements",
                        license: "license1",
                        userId: 4
                    });
                }
                expect(response.status).toBe(429);
            }));
            it('should return status 492 for get method after consuming all burst but 201 for post as priority of post is higher for license 1', () => __awaiter(void 0, void 0, void 0, function* () {
                let getresponse, postResponse;
                for (let i = 0; i < 7; ++i) {
                    getresponse = yield supertest_1.default(server).get('/api/disbursement').set({
                        platform: "Disbursements",
                        license: "license1",
                        userId: 5
                    });
                }
                for (let i = 0; i < 2; ++i) {
                    postResponse = yield supertest_1.default(server).post('/api/disbursement').set({
                        platform: "Disbursements",
                        license: "license1",
                        userId: 5
                    });
                }
                expect(getresponse.status).toBe(429);
                expect(postResponse.status).toBe(201);
                ;
            }));
        });
        describe('For License 2 it should serve less request as compared to license 1', () => {
            it('should return status 200 if we send equal or less number of request specifed for get method for license2', () => __awaiter(void 0, void 0, void 0, function* () {
                const response = yield supertest_1.default(server).get('/api/disbursement').set({
                    platform: "Disbursements",
                    license: "license1",
                    userId: 1
                });
                expect(response.status).toBe(200);
            }));
            it('should return status 492 if we send more number of request specifed for get method  for license2', () => __awaiter(void 0, void 0, void 0, function* () {
                let response;
                for (let i = 0; i < 4; ++i) {
                    response = yield supertest_1.default(server).get('/api/disbursement').set({
                        platform: "Disbursements",
                        license: "license1",
                        userId: 2
                    });
                }
                expect(response.status).toBe(429);
            }));
            it('should return status 201 if we send equal or less number of request specifed for post method  for license2', () => __awaiter(void 0, void 0, void 0, function* () {
                const response = yield supertest_1.default(server).post('/api/disbursement').set({
                    platform: "Disbursements",
                    license: "license1",
                    userId: 3
                });
                expect(response.status).toBe(201);
            }));
            it('should return status 492 if we send more number of request specifed for post method  for license2', () => __awaiter(void 0, void 0, void 0, function* () {
                let response;
                for (let i = 0; i < 7; ++i) {
                    response = yield supertest_1.default(server).post('/api/disbursement').set({
                        platform: "Disbursements",
                        license: "license1",
                        userId: 4
                    });
                }
                expect(response.status).toBe(429);
            }));
            it('should return status 492 for get method after consuming all burst but 201 for post as priority of post is higher  for license2', () => __awaiter(void 0, void 0, void 0, function* () {
                let getresponse, postResponse;
                for (let i = 0; i < 4; ++i) {
                    getresponse = yield supertest_1.default(server).get('/api/disbursement').set({
                        platform: "Disbursements",
                        license: "license1",
                        userId: 5
                    });
                }
                for (let i = 0; i < 1; ++i) {
                    postResponse = yield supertest_1.default(server).post('/api/disbursement').set({
                        platform: "Disbursements",
                        license: "license1",
                        userId: 5
                    });
                }
                expect(getresponse.status).toBe(429);
                expect(postResponse.status).toBe(201);
                ;
            }));
        });
    });
});
//# sourceMappingURL=disbursement.test.js.map