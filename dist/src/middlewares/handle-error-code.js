"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorHandler = void 0;
const lodash_1 = __importDefault(require("lodash"));
const logger_1 = require("src/util/logger");
const errors_1 = require("src/util/errors");
exports.errorHandler = () => {
    // This is an express error handler, need to the 4 variable signature
    // eslint-disable-next-line
    return (err, req, res, next) => {
        if (err.status) {
            logger_1.logger.info({ err }, 'Validation Error');
            return res.status(err.status).json({
                message: err.message,
                error_code: err.error_code || errors_1.ErrorCodes.API_VALIDATION_ERROR,
                errors: err.errors
            });
        }
        const statusCode = errors_1.ErrorCodeMap[err.error_code];
        if (lodash_1.default.isNumber(statusCode)) {
            const logContext = {
                error_code: err.error_code,
                status_code: statusCode,
                context: err.context
            };
            // _.defaults(logContext, req.safeLoggingRequestData); // to be determined what is this for
            logger_1.logger.info(logContext, 'API error');
            return res.status(statusCode).send({
                error_code: err.error_code,
                message: err.message
            });
        }
        logger_1.logger.error(err, 'unexpected error');
        return res.status(500).send({
            error_code: 'SERVER_ERROR',
            message: 'Something unexpected happened, we are investigating this issue right now'
        });
    };
};
//# sourceMappingURL=handle-error-code.js.map