"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.rateLimiter = void 0;
const rateLimiter_1 = require("src/util/rateLimiter");
const setHeaders_1 = require("src/util/setHeaders");
exports.rateLimiter = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const rateLimiterInstance = rateLimiter_1.RateLimiter.getInstance();
    const { platform, license, userid } = req.headers;
    const { method } = req;
    const rateLimiterFunction = rateLimiterInstance.getRateLimiter(platform.toString(), license.toString(), method);
    if (rateLimiterFunction === undefined) {
        res.status(500).json({
            error_code: 'INTERNAL_SERVER_ERROR',
            message: 'Platform or user not known',
            errors: []
        });
    }
    const key = platform + '.' + license + '.' + userid + '.' + method;
    rateLimiterFunction.consume(key)
        .then((rateLimiterParameter) => {
        req.body.rateLimiterParameter = rateLimiterParameter;
        next();
    })
        .catch((rateLimiterParameter) => {
        const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } = rateLimiterParameter;
        setHeaders_1.setHeaders(res, remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration);
        res.status(429).json({
            error_code: 'Rate_Limiting_Error',
            message: 'Rate Limit Exceeded',
            errors: [],
            rateLimiterParameter: rateLimiterParameter
        });
    });
});
//# sourceMappingURL=rateLimiter.js.map