"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.init = void 0;
require("reflect-metadata");
const disbursement_1 = require("src/controllers/disbursement");
const fixedVA_1 = require("src/controllers/fixedVA");
const invoice_1 = require("src/controllers/invoice");
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        const disbursementController = new disbursement_1.DisbursementController();
        const fixedVAController = new fixedVA_1.FixedVAController();
        const invoiceController = new invoice_1.InvoiceController();
        return {
            disbursementController,
            fixedVAController,
            invoiceController
        };
    });
}
exports.init = init;
//# sourceMappingURL=init.js.map