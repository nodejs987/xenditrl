"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
const isTestEnv = process.env.NODE_ENV === 'test';
exports.logger = {
    info: (context, message) => {
        if (!isTestEnv)
            console.log(context, message);
    },
    warn: (context, message) => {
        if (!isTestEnv)
            console.log(context, message);
    },
    error: (err, message) => {
        if (!isTestEnv)
            console.error(err, message);
    }
};
//# sourceMappingURL=logger.js.map