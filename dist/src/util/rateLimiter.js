"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RateLimiter = void 0;
const rate_limiter_flexible_1 = require("rate-limiter-flexible");
const rateLimiterConfig_1 = require("src/config/rateLimiterConfig");
class RateLimiter {
    constructor() {
        this.rateLimiterFunctionsMap = {};
    }
    static getInstance() {
        return this.instance || (this.instance = new RateLimiter());
    }
    getRateLimiter(platform, user, method) {
        const { rateLimiterName, rateLimiterconfig } = rateLimiterConfig_1.loadConfig(platform, user, method);
        if (rateLimiterName === undefined || rateLimiterconfig === undefined)
            return undefined;
        else if (this.rateLimiterFunctionsMap.hasOwnProperty(rateLimiterName)) {
            return this.rateLimiterFunctionsMap[rateLimiterName];
        }
        else {
            return this.createRateLimiter(rateLimiterName, rateLimiterconfig);
        }
    }
    createRateLimiter(rateLimiterName, rateLimiterconfig) {
        const rateLimiterMemory = new rate_limiter_flexible_1.RateLimiterMemory(rateLimiterconfig.configInterval);
        this.rateLimiterFunctionsMap[rateLimiterName] = rateLimiterMemory;
        return this.rateLimiterFunctionsMap[rateLimiterName];
    }
}
exports.RateLimiter = RateLimiter;
//# sourceMappingURL=rateLimiter.js.map