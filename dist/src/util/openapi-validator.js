"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenApiValidator = void 0;
const express_openapi_validator_1 = require("express-openapi-validator");
const openapi_spec_loader_1 = require("express-openapi-validator/dist/framework/openapi.spec.loader");
const ajv_1 = require("express-openapi-validator/dist/framework/ajv");
class OpenApiValidator {
    constructor(opts = {
        apiSpec: './docs/openapi.yaml',
        validateRequests: true,
        validateResponses: true,
        unknownFormats: ['uuid']
    }) {
        this.opts = opts;
        this.openApiValidator = new express_openapi_validator_1.OpenApiValidator(opts);
    }
    build() {
        return __awaiter(this, void 0, void 0, function* () {
            const spec = yield new openapi_spec_loader_1.OpenApiSpecLoader({ apiDoc: this.opts.apiSpec }).load();
            this.manualValidator = ajv_1.createResponseAjv(spec.apiDoc, {
                nullable: true,
                removeAdditional: false,
                useDefaults: true,
                unknownFormats: this.opts.unknownFormats
            });
            return this;
        });
    }
    install(app) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.openApiValidator.install(app);
        });
    }
    /**
     * Validate data using schema
     * schemas have been loaded during construction
     * @param  {String} schemaName name of the schema under #/components/schemas
     * @param  {Any} data to be validated
     * @return {ValidationResult} validation result
     * @throws {Error}
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    validate(schemaName, data) {
        if (!this.manualValidator) {
            throw new Error('OpenApiValidator needs to be build() first for manual validation');
        }
        const v = this.manualValidator.getSchema(`#/components/schemas/${schemaName}`);
        if (!v) {
            throw new Error(`no schema with name "${schemaName}"`);
        }
        const isValid = v(data);
        return {
            isValid,
            errors: v.errors
        };
    }
}
exports.OpenApiValidator = OpenApiValidator;
//# sourceMappingURL=openapi-validator.js.map