"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvoiceController = void 0;
const express_1 = require("express");
const setHeaders_1 = require("src/util/setHeaders");
class InvoiceController {
    constructor() {
        this.router = express_1.Router();
        this.router.get('/', this.getInvoice.bind(this));
        this.router.post('/', this.postInvoice.bind(this));
    }
    getRouter() {
        return this.router;
    }
    getInvoice(req, res, next) {
        const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } = req.body.rateLimiterParameter;
        setHeaders_1.setHeaders(res, remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration);
        res.status(200).send({
            "message": "Hi From Invoice team",
            "rateLimiterParameter": req.body.rateLimiterParameter
        });
    }
    postInvoice(req, res, next) {
        const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } = req.body.rateLimiterParameter;
        setHeaders_1.setHeaders(res, remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration);
        res.status(201).send({
            "message": "Hi From Invoice team",
            "rateLimiterParameter": req.body.rateLimiterParameter
        });
    }
}
exports.InvoiceController = InvoiceController;
//# sourceMappingURL=invoice.js.map