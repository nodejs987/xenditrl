"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FixedVAController = void 0;
const express_1 = require("express");
const setHeaders_1 = require("src/util/setHeaders");
class FixedVAController {
    constructor() {
        this.router = express_1.Router();
        this.router.get('/', this.getFixedVA.bind(this));
        this.router.post('/', this.postFixedVA.bind(this));
    }
    getRouter() {
        return this.router;
    }
    getFixedVA(req, res, next) {
        const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } = req.body.rateLimiterParameter;
        setHeaders_1.setHeaders(res, remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration);
        res.status(200).send({
            "message": "Hi From FixedVA team",
            "rateLimiterParameter": req.body.rateLimiterParameter
        });
    }
    postFixedVA(req, res, next) {
        const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } = req.body.rateLimiterParameter;
        setHeaders_1.setHeaders(res, remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration);
        res.status(201).send({
            "message": "Hi From FixedVA team",
            "rateLimiterParameter": req.body.rateLimiterParameter
        });
    }
}
exports.FixedVAController = FixedVAController;
//# sourceMappingURL=fixedVA.js.map