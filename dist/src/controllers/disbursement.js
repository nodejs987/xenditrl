"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DisbursementController = void 0;
const express_1 = require("express");
const setHeaders_1 = require("src/util/setHeaders");
class DisbursementController {
    constructor() {
        this.router = express_1.Router();
        this.router.get('/', this.getDisbursement.bind(this));
        this.router.post('/', this.postDisbursement.bind(this));
    }
    getRouter() {
        return this.router;
    }
    getDisbursement(req, res, next) {
        const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } = req.body.rateLimiterParameter;
        setHeaders_1.setHeaders(res, remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration);
        res.status(200).send({
            "message": "Hi From disbursement team",
            "rateLimiterParameter": req.body.rateLimiterParameter
        });
    }
    postDisbursement(req, res, next) {
        const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } = req.body.rateLimiterParameter;
        setHeaders_1.setHeaders(res, remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration);
        res.status(201).send({
            "message": "Hi From disbursement team",
            "rateLimiterParameter": req.body.rateLimiterParameter
        });
    }
}
exports.DisbursementController = DisbursementController;
//# sourceMappingURL=disbursement.js.map