import express, { Application } from "express";
import httpContext from "express-http-context";
import compression from "compression";
import { init } from "src/init";
import { OpenApiValidator } from "src/util/openapi-validator";
import { errorHandler } from "src/middlewares/handle-error-code";
import { rateLimiter } from "src/middlewares/rate-limiter";

async function setRoutes(app: Application) {
  const { disbursementController, invoiceController, fixedVAController } =
    await init();

  app.use("/api/disbursement", disbursementController.getRouter());
  app.use("/api/invoice", invoiceController.getRouter());
  app.use("/api/fixedVA", fixedVAController.getRouter());
}

export async function createApp(): Promise<express.Application> {
  const app = express();
  app.set("port", process.env.port || 3000);
  app.use(compression());
  app.use(express.json({ limit: "5mb", type: "application/json" }));
  app.use(express.urlencoded({ extended: true }));

  const openApiValidator = new OpenApiValidator();
  await openApiValidator.install(app);

  app.use(httpContext.middleware);
  app.use(errorHandler());
  app.use(rateLimiter);
  await setRoutes(app);

  return app;
}
