const platformRateLimiterConfig: any = {
  Disbursements: {
    license1: {
      GET: { configInterval: { points: 2, duration: 10 } },
      POST: { configInterval: { points: 3, duration: 10 } },
    },
    license2: {
      GET: { configInterval: { points: 1, duration: 10 } },
      POST: { configInterval: { points: 2, duration: 10 } },
    },
  },
  FixedVA: {
    license1: {
      GET: { configInterval: { points: 2, duration: 10 } },
      POST: { configInterval: { points: 3, duration: 10 } },
    },
    license2: {
      GET: { configInterval: { points: 1, duration: 10 } },
      POST: { configInterval: { points: 2, duration: 10 } },
    },
  },
  Invoice: {
    license1: {
      GET: { configInterval: { points: 2, duration: 10 } },
      POST: { configInterval: { points: 3, duration: 10 } },
    },
    license2: {
      GET: { configInterval: { points: 1, duration: 10 } },
      POST: { configInterval: { points: 2, duration: 10 } },
    },
  },
};

export function loadConfig(platform: string, user: string, method: string) {
  const rateLimiterName: string =
    platformRateLimiterConfig.hasOwnProperty(platform) &&
    platformRateLimiterConfig[platform].hasOwnProperty(user)
      ? `${platform}.${user}.${method}`
      : undefined;
  const rateLimiterconfig =
    platformRateLimiterConfig.hasOwnProperty(platform) &&
    platformRateLimiterConfig[platform].hasOwnProperty(user)
      ? platformRateLimiterConfig[platform][user][method]
      : undefined;

  return {
    rateLimiterName,
    rateLimiterconfig,
  };
}
