import "reflect-metadata";
import { DisbursementController } from "src/controllers/disbursement";
import { FixedVAController } from "src/controllers/fixed-va";
import { InvoiceController } from "src/controllers/invoice";

export async function init() {
  const disbursementController = new DisbursementController();
  const fixedVAController = new FixedVAController();
  const invoiceController = new InvoiceController();

  return {
    disbursementController,
    fixedVAController,
    invoiceController,
  };
}
