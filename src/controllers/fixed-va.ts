import { Router, Request, Response, NextFunction } from "express";
import { setHeaders } from "src/util/set-headers";

export class FixedVAController {
  private router: Router;

  public constructor() {
    this.router = Router();
    this.router.get("/", this.getFixedVA.bind(this));
    this.router.post("/", this.postFixedVA.bind(this));
  }

  getRouter() {
    return this.router;
  }

  getFixedVA(req: Request, res: Response, next: NextFunction) {
    const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } =
      req.body.rateLimiterParameter;
    setHeaders(
      res,
      remainingPoints,
      msBeforeNext,
      consumedPoints,
      isFirstInDuration
    );
    res.status(200).send({
      message: "Hi From FixedVA team",
      rateLimiterParameter: req.body.rateLimiterParameter,
    });
  }

  postFixedVA(req: Request, res: Response, next: NextFunction) {
    const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } =
      req.body.rateLimiterParameter;
    setHeaders(
      res,
      remainingPoints,
      msBeforeNext,
      consumedPoints,
      isFirstInDuration
    );
    res.status(201).send({
      message: "Hi From FixedVA team",
      rateLimiterParameter: req.body.rateLimiterParameter,
    });
  }
}
