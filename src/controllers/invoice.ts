import { Router, Request, Response, NextFunction } from "express";
import { setHeaders } from "src/util/set-headers";

export class InvoiceController {
  private router: Router;

  public constructor() {
    this.router = Router();
    this.router.get("/", this.getInvoice.bind(this));
    this.router.post("/", this.postInvoice.bind(this));
  }

  getRouter() {
    return this.router;
  }

  getInvoice(req: Request, res: Response, next: NextFunction) {
    const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } =
      req.body.rateLimiterParameter;
    setHeaders(
      res,
      remainingPoints,
      msBeforeNext,
      consumedPoints,
      isFirstInDuration
    );
    res.status(200).send({
      message: "Hi From Invoice team",
      rateLimiterParameter: req.body.rateLimiterParameter,
    });
  }

  postInvoice(req: Request, res: Response, next: NextFunction) {
    const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } =
      req.body.rateLimiterParameter;
    setHeaders(
      res,
      remainingPoints,
      msBeforeNext,
      consumedPoints,
      isFirstInDuration
    );
    res.status(201).send({
      message: "Hi From Invoice team",
      rateLimiterParameter: req.body.rateLimiterParameter,
    });
  }
}
