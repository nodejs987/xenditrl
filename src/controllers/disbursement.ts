import { Router, Request, Response, NextFunction } from "express";
import { setHeaders } from "src/util/set-headers";

export class DisbursementController {
  private router: Router;

  public constructor() {
    this.router = Router();
    this.router.get("/", this.getDisbursement.bind(this));
    this.router.post("/", this.postDisbursement.bind(this));
  }

  getRouter() {
    return this.router;
  }

  getDisbursement(req: Request, res: Response, next: NextFunction) {
    const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } =
      req.body.rateLimiterParameter;
    setHeaders(
      res,
      remainingPoints,
      msBeforeNext,
      consumedPoints,
      isFirstInDuration
    );
    res.status(200).send({
      message: "Hi From disbursement team",
      rateLimiterParameter: req.body.rateLimiterParameter,
    });
  }

  postDisbursement(req: Request, res: Response, next: NextFunction) {
    const { remainingPoints, msBeforeNext, consumedPoints, isFirstInDuration } =
      req.body.rateLimiterParameter;
    setHeaders(
      res,
      remainingPoints,
      msBeforeNext,
      consumedPoints,
      isFirstInDuration
    );
    res.status(201).send({
      message: "Hi From disbursement team",
      rateLimiterParameter: req.body.rateLimiterParameter,
    });
  }
}
