import { NextFunction, Request, Response } from "express";
import { RateLimiter } from "src/util/rate-limiter";
import { setHeaders } from "src/util/set-headers";

export const rateLimiter = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const rateLimiterInstance = RateLimiter.getInstance();
  const { platform, license, userid } = req.headers;

  const { method } = req;

  const rateLimiterFunction = rateLimiterInstance.getRateLimiter(
    platform.toString(),
    license.toString(),
    method
  );

  if (rateLimiterFunction === undefined) {
    res.status(500).json({
      error_code: "INTERNAL_SERVER_ERROR",
      message: "Platform or user not known",
      errors: [],
    });
  }

  const key = `${platform}.${license}.${userid}.${method}`;

  rateLimiterFunction
    .consume(key)
    .then((rateLimiterParameter: any) => {
      req.body.rateLimiterParameter = rateLimiterParameter;
      next();
    })
    .catch((rateLimiterParameter: any) => {
      const {
        remainingPoints,
        msBeforeNext,
        consumedPoints,
        isFirstInDuration,
      } = rateLimiterParameter;
      setHeaders(
        res,
        remainingPoints,
        msBeforeNext,
        consumedPoints,
        isFirstInDuration
      );
      res.status(429).json({
        error_code: "Rate_Limiting_Error",
        message: "Rate Limit Exceeded",
        errors: [],
        rateLimiterParameter,
      });
    });
};
