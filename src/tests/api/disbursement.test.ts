import supertest from "supertest";

import { createApp } from "src/app";

describe("Disbursement Integration tests", () => {
  let server: Express.Application;

  beforeAll(async () => {
    server = await createApp();
  });

  describe("/api/disbursement", () => {
    describe("For License 1 it should server more request as compared to license 2", () => {
      it("should return status 200 if we send equal or less number of request specifed for get method for license 1", async () => {
        const response = await supertest(server).get("/api/disbursement").set({
          platform: "Disbursements",
          license: "license1",
          userId: 1,
        });
        expect(response.status).toBe(200);
      });
      it("should return status 492 if we send more number of request specifed for get method for license 1", async () => {
        let response;
        for (let i = 0; i < 7; i += 1) {
          response = await supertest(server).get("/api/disbursement").set({
            platform: "Disbursements",
            license: "license1",
            userId: 2,
          });
        }
        expect(response.status).toBe(429);
      });
      it("should return status 201 if we send equal or less number of request specifed for post method for license 1", async () => {
        const response = await supertest(server).post("/api/disbursement").set({
          platform: "Disbursements",
          license: "license1",
          userId: 3,
        });
        expect(response.status).toBe(201);
      });
      it("should return status 492 if we send more number of request specifed for post method for license 1", async () => {
        let response;
        for (let i = 0; i < 11; i += 1) {
          response = await supertest(server).post("/api/disbursement").set({
            platform: "Disbursements",
            license: "license1",
            userId: 4,
          });
        }
        expect(response.status).toBe(429);
      });
      it("should return status 492 for get method after consuming all burst but 201 for post as priority of post is higher for license 1", async () => {
        let getresponse;
        let postResponse;
        for (let i = 0; i < 7; i += 1) {
          getresponse = await supertest(server).get("/api/disbursement").set({
            platform: "Disbursements",
            license: "license1",
            userId: 5,
          });
        }
        for (let i = 0; i < 2; i += 1) {
          postResponse = await supertest(server).post("/api/disbursement").set({
            platform: "Disbursements",
            license: "license1",
            userId: 5,
          });
        }
        expect(getresponse.status).toBe(429);
        expect(postResponse.status).toBe(201);
      });
    });
    describe("For License 2 it should serve less request as compared to license 1", () => {
      it("should return status 200 if we send equal or less number of request specifed for get method for license2", async () => {
        const response = await supertest(server).get("/api/disbursement").set({
          platform: "Disbursements",
          license: "license1",
          userId: 1,
        });
        expect(response.status).toBe(200);
      });
      it("should return status 492 if we send more number of request specifed for get method  for license2", async () => {
        let response;
        for (let i = 0; i < 4; i += 1) {
          response = await supertest(server).get("/api/disbursement").set({
            platform: "Disbursements",
            license: "license1",
            userId: 2,
          });
        }
        expect(response.status).toBe(429);
      });
      it("should return status 201 if we send equal or less number of request specifed for post method  for license2", async () => {
        const response = await supertest(server).post("/api/disbursement").set({
          platform: "Disbursements",
          license: "license1",
          userId: 3,
        });
        expect(response.status).toBe(201);
      });
      it("should return status 492 if we send more number of request specifed for post method  for license2", async () => {
        let response;
        for (let i = 0; i < 7; i += 1) {
          response = await supertest(server).post("/api/disbursement").set({
            platform: "Disbursements",
            license: "license1",
            userId: 4,
          });
        }
        expect(response.status).toBe(429);
      });
      it("should return status 492 for get method after consuming all burst but 201 for post as priority of post is higher  for license2", async () => {
        let getresponse;
        let postResponse;
        for (let i = 0; i < 4; i += 1) {
          getresponse = await supertest(server).get("/api/disbursement").set({
            platform: "Disbursements",
            license: "license1",
            userId: 5,
          });
        }
        for (let i = 0; i < 1; i += 1) {
          postResponse = await supertest(server).post("/api/disbursement").set({
            platform: "Disbursements",
            license: "license1",
            userId: 5,
          });
        }
        expect(getresponse.status).toBe(429);
        expect(postResponse.status).toBe(201);
      });
    });
  });
});
