import { Response } from "express";

export function setHeaders(
  res: Response,
  remainingPoints: number,
  msBeforeNext: number,
  consumedPoints: number,
  isFirstInDuration: boolean
) {
  res.setHeader("Remaining-Points", remainingPoints);
  res.setHeader("MS-Before-Next", msBeforeNext);
  res.setHeader("Consumed-Points", consumedPoints);
  res.setHeader("Is-First-In-Duration", isFirstInDuration.toString());
}
