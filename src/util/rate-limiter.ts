import { RateLimiterMemory } from "rate-limiter-flexible";
import { loadConfig } from "src/config/rate-limiter-config";

export class RateLimiter {
  private rateLimiterFunctionsMap: any = {};

  private static instance: RateLimiter;

  public static getInstance() {
    if (this.instance) return this.instance;
    this.instance = new RateLimiter();
    return this.instance;
  }

  getRateLimiter(platform: string, user: string, method: string) {
    const { rateLimiterName, rateLimiterconfig } = loadConfig(
      platform,
      user,
      method
    );
    if (rateLimiterName === undefined || rateLimiterconfig === undefined)
      return undefined;
    if (this.rateLimiterFunctionsMap.hasOwnProperty(rateLimiterName)) {
      return this.rateLimiterFunctionsMap[rateLimiterName];
    }
    return this.createRateLimiter(rateLimiterName, rateLimiterconfig);
  }

  createRateLimiter(rateLimiterName: string, rateLimiterconfig: any) {
    const rateLimiterMemory = new RateLimiterMemory(
      rateLimiterconfig.configInterval
    );
    this.rateLimiterFunctionsMap[rateLimiterName] = rateLimiterMemory;
    return this.rateLimiterFunctionsMap[rateLimiterName];
  }
}
